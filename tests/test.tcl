source ../vmdplugin/longbondeliminator.tcl
package require longbondeliminator
::longbondeliminator::minimize aggrecan/system_min_fix.namd 3 "namd2 +p16" 0.42 0 1
::longbondeliminator::minimize lignin/minimize.namd 3 "namd2 +p4" 0.4 0 1
::longbondeliminator::minimize virus/minimize.namd 3 "namd2 +p16" 0.4 0 1
exit
