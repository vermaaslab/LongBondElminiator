# LongBondEliminator User Guide

LongBondEliminator (LBE for short) is a VMD plugin that is designed to facilitate the minimization and simulation of molecular systems that contain ring piercings or penetrations.
In these systems, a bond is stretched by having atoms on either side of a ring, creating an unphysical geometry.
A simple example might be ethane split across a benzene ring, like this: ![ethane bifurcation](piercedbenzene.png)
LBE uses various methods build into NAMD to relax away these stretched bonds.
The methodology is described in more detail in a [journal article](https://www.mdpi.com/2218-273X/13/1/107)

## Installation and Dependencies

Since LongBondEliminator depends intimately on VMD and NAMD, both programs will need to be installed prior to use.
Instructions for doing so can be found on the [VMD](https://www.ks.uiuc.edu/Research/vmd/current/ug/) and [NAMD user guides](https://www.ks.uiuc.edu/Research/namd/current/ug/).

The easiest way to install the VMD plugin aspect of LongBondEliminator is to download this repository, and make VMD aware of its existence.
Downloading the repository can be done via `git clone` or zip file downloads available at [gitlab](https://gitlab.msu.edu/vermaaslab/LongBondElminiator).
The downloaded repository is assumed to be in your home directory, which for the purposes of this example, we will call `/home/user/LongBondEliminator`.
Once downloaded, we append the plugin directory to the `auto_path` tcl variable within VMD, so that the plugin can be found. In the TkConsole, this is done via the following commands:
```tcl
lappend auto_path /home/user/LongBondElminiator/vmdplugin
package require longbondeliminator
```
If `package require longbondeliminator` returns a version number, the plugin is loaded successfully, and can be used to minimize long bonds.
To install LongBondEliminator so that it is recognized every time VMD starts, the `lappend auto_path` line should be added to your .vmdrc file, which contains configuration and settings information for VMD. For more information about .vmdrc files, please see the [VMD userguide](https://www.ks.uiuc.edu/Research/vmd/current/ug/ug.html) (scroll down towards the end of the page).

## Usage


### Minimization
What LBE depends on is an existing NAMD configuration file that it will modify to go through the LBE minimization procedure, which we assume will be called `minimize.namd`.
To run the LBE process on the system that would be minimized by `minimize.namd`, a basic call would look something like this:
```tcl
::longbondeliminator::minimize minimize.namd 3
```
The `3` here refers to the number of lines that will be removed from `minimize.namd` when creating `minimize-lbe.namd`.
`minimize-lbe.namd` is the new NAMD configuration file that will be run during the LBE process, which may feature alchemical methods as well as collective variables, which may restrict the valid NAMD versions that can run LBE.
As an example, CUDA builds of NAMD 2.14 do not have the ability to run alchemical simulations, and thus CPU-based NAMD binaries may be required.
LBE will successively iterate through its workflow to generate an optimal configuration, which will be written to `lbe-opt.coor` to check that the ring piercing artifacts are indeed dealt with.

The `minimize` function has additional optional arguments as well, including the arguments used to run the NAMD simulation (e.g. `namd2 +p16`. The default is `namd2`), the chosen cutoff for how stretched a bond is allowed to be (0.4 is the default), an integer to flag if chirality restraints should be added (they are by default, 0 means not to generate them), and an integer to flag if intermediate coordinate frames should be saved for posterity (default is not to).
A fully loaded command line would then look like this:
```tcl
::longbondeliminator::minimize minimize.namd 3 "namd2 +p16" 0.4 0 1
```
This would use 16 processors to run NAMD2, with the default cutoffs, not to generate chirality constraints, and to save intermediates.
This example is pulled straight from `tests/test.tcl`.


### Helper functions

For analysis and visualization purposes, LBE has two helper functions, `writelargestdeviation` and `tagdeviation`.
Examples of how to use the functions are given in `testoutput/Analysis/analyzetraj.tcl`, although the `tagdeviation` function is commented out because of how slow it can be.

`writelargestdeviation` will write out the bond with the largest deviation from the equilibrium bond length from a loaded molecule in VMD.
The function needs only three arguments: the molecule ID to be analyzed, the list of parameter files that have equilibrium bond length information, and a filename for the newly written file.
Each line for the file will have the frame number, the largest deviation amount from the equilibrium bond length, and the two atomic indices for the stretched bond.

`tagdeviation` has similar arguments (molecule ID and list of parameter files), but will output the sum of the bond deviations into the `user` field within VMD.
Since the `user` field is a per-frame field, this method can be combined with VMD coloration methods to develop a sense of how the ring penetrations changed over time.

## What is in this gitlab repository

There are 3 directories in the gitlab repository

- `vmdplugin`, which contains the plugin itself
- `tests`, which contains a discrete set of broken structures to test LBE methods on.
- `testoutput`, which contains the output if you run the contents of `tests`
