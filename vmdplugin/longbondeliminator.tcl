package provide longbondeliminator 1.0

namespace eval ::longbondeliminator:: {
	package require topotools
	package require exectool
	package require pbctools
	proc parseinputfile { namdconfigfile } {
		#Need to get structure, coordinates, and parameter files.
		set fin [open $namdconfigfile r]
		set lines [split [read $fin] "\n"]
		close $fin
		set structure ""
		set coordinates ""
		set outputname ""
		set parameterlist [list ]
		foreach line $lines {
			# Split based on whitespace.
			#The internal subst does variable substitution
			set s [regexp -inline -all -- {\S+} [subst $line]]
			if { [llength $s] > 1 } {
				set firstword [string tolower [lindex $s 0]]
				switch $firstword {
					parameters { lappend parameterlist "[lindex $s 1]" }
					structure {
						set structure [lindex $s 1]
					}
					coordinates {
						if { $coordinates == "" } {
							set coordinates [lindex $s 1]
						}
					}
					bincoordinates {set coordinates [lindex $s 1]}
					outputname { set outputname [lindex $s 1]}
					#The set handles setting variables. Useful for the lignin case.
					set { 
						eval $line
					}
				}
			}
		}
		return [list $structure $coordinates $parameterlist $outputname ]
	}
	proc loadsystem { structure coordinates } {
		set mid [mol new]
		mol addfile $structure molid $mid
		#If structure ends up being a js file, it *could* come with coordinates of its own.
		animate delete beg 0 end 0 $mid
		mol addfile $coordinates molid $mid
		return $mid
	}
	proc buildbondtable { filelist } {
		set bonddict [dict create]
		foreach paramfile $filelist {
	        set fin [open $paramfile r]
	        set fdat [read $fin]
	        close $fin
	        set data [split $fdat "\n"]
	        foreach line $data {
	            # Try to find a comment character.
	            # If found, discard the remainder of the line.
	            set idx [string first ! $line]
	            # Subtract one here, since if found, we don't
	            # want to include it in the substring.
	            incr idx -1
	            if {$idx < 0} {
	                set idx end
	            }
	            set l [string range $line 0 $idx]
	            # Split based on whitespace.
	            set ss [regexp -inline -all -- {\S+} $l]
	            # Fit the split to one of the (type aware) parameter parsings,
	            # ignore it if it doesn't fit.
	            switch [llength $ss] {
	                4 {
	                	if {[string is alnum [lindex $ss 0]] && [string is ascii [lindex $ss 0]] &&
                              [string is alnum [lindex $ss 1]] && [string is ascii [lindex $ss 1]] &&
                              [string is double [lindex $ss 2]] &&
                              [string is double [lindex $ss 3]]} {
                            set key [join [lsort [ list [lindex $ss 0] [lindex $ss 1]]] "_"]
                            set val [lindex $ss 3]
	                        dict set bonddict $key $val
	                    }
	                }
	            }
	        }
	    }
	    return $bonddict
	}
	proc minimizationscriptname { namdconfigfile } {
		set confroot [file rootname $namdconfigfile]
		set confext [file extension $namdconfigfile]
		return "$confroot-lbe$confext"
	}
	proc genchiralityextrabondsfile { mid filename { extrabondsk 200 }} {
		set fout [open $filename w]
		#Chiral centers have 4 bonds.
		set centralatoms [atomselect $mid "numbonds == 4"]
		puts [$centralatoms num]
		foreach cidx [$centralatoms get index] {
			set neighbors [atomselect $mid "(not index $cidx) and withinbonds 1 of index $cidx"]
			lassign [$neighbors get index] id0 id1 id2 id3
			set impr [measure imprp [list $id0 $id1 $id2 $id3] molid $mid]
      		set impr2 [measure imprp [list $cidx $id1 $id2 $id3] molid $mid]
      		puts $fout "improper $id0 $id1 $id2 $id3 $extrabondsk $impr"
      		puts $fout "improper $cidx $id1 $id2 $id3 $extrabondsk $impr2"
      		$neighbors delete
      		puts $cidx
		}
		close $fout
	}
	proc writenamdconf { namdconfigfile deletelines } {
		set fin [open $namdconfigfile r]
	    set fdat [read $fin]
	    close $fin
	    set data [split $fdat "\n"]
	    set data [lrange $data 0 end-$deletelines]
	    set fout [open [ minimizationscriptname $namdconfigfile ] w]
	    puts $fout [join $data "\n"]
	    return $fout
	}
	proc forcelongestbond { mid indices } {
		set sel [atomselect $mid "index $indices"]
		set com [measure center $sel]
		set randvec [list ]
		foreach el [list x y z] {
			lappend randvec [expr { rand() }]
		}
		set bondvec [vecsub [lindex [$sel get {x y z}] 0] [lindex [$sel get {x y z}] 1]]
		#Taking the cross product gives a vector perpendicular to both the bond vector and the random vector.
		set crossvec [veccross $bondvec $randvec]
		set crossvec [vecscale [expr {10.0/[veclength $crossvec]}] $crossvec]
		$sel delete
		set vec [vecadd $com $crossvec]
		return " ( [lindex $vec 0] , [lindex $vec 1] , [lindex $vec 2] ) "
	}
	proc runloop { namdconfigfile deletelines mid bonddict namdrunargs outputname precision genchirality writeintemediates } {
		set repulsion 0.1
		set gridcoupling 10
		set fout [writenamdconf $namdconfigfile $deletelines]
		puts $fout "minimize 500"
		close $fout
		if { $genchirality } {
			genchiralityextrabondsfile $mid lbe-extrabonds.txt
		}
		set ss [regexp -inline -all -- {\S+} $namdrunargs]
		set namdbin [lindex $ss 0]
		set namdargs [join [lrange $ss 1 end] " "]
		set asel [atomselect $mid "all"]
		$asel set occupancy 0
		$asel set beta 0
		set atypes [$asel get type]
		#mdffi sim $asel -o grid.dx -res 10 -spacing 1
		set finished 0
		set counter 0
		set minmax [measure minmax $asel]
		set pbcval [pbc set [vecsub [lindex $minmax 1] [lindex $minmax 0]]]
		set othersel [atomselect $mid "(within 4 of occupancy > 0) and (not withinbonds 3 of occupancy > 0)"]
		set badbeta [atomselect $mid "beta > 0"]
		set cutoff $precision
		set largestloc ""
		while { ( ! $finished ) || ( $largestdeviation > $precision ) } {
			::ExecTool::exec $namdbin $namdargs [ minimizationscriptname $namdconfigfile ] > [ file rootname $namdconfigfile ].log
			animate delete all $mid
			incr counter
			incr finished
			mol addfile $outputname.coor type namdbin waitfor all molid $mid
			set unfinished [vecsum [$asel get beta]]
			$asel set beta 0
			$asel set occupancy 0
			set largestdeviation 0.0
			set oldlargestloc $largestloc
			foreach bond [topo getbondlist -molid $mid] {
				set bondkey [join [lsort [ list [lindex $atypes [lindex $bond 0]] [lindex $atypes [lindex $bond 1]]]] "_"]
				set bonddiff [expr { [measure bond $bond molid $mid] - [dict get $bonddict $bondkey] } ]
				if { $bonddiff > $largestdeviation } {
					set largestdeviation $bonddiff
					set largestloc $bond
				}
				if { $bonddiff > $cutoff } {
					set ssel [atomselect $mid "same residue as index $bond"]
					$ssel set beta 1
					$ssel set occupancy 1
					set finished 0
					$ssel delete
				}
			}
			puts "Largest deviation from equilibrium bond lengths: $largestdeviation"
			puts "Largest deviation between atoms: $largestloc"
			if { $oldlargestloc == $largestloc } {
				puts "Increasing force constants to dislodge stuck longest bond."
				set wallconstant [expr {$wallconstant * 2.0}]
				set gc [expr { $gc * 2.0 }]
			} else {
				set wallconstant [expr {$repulsion / $cutoff}]
				set gc [expr { $gridcoupling * $cutoff }]
			}
			#If we are not finished, there is currently a long bond, and so we want TI + grid forces.
			#If we are unfinished, we just want to run again without the extras.
			if { ! $finished || $unfinished } {
				$othersel update
				$othersel set occupancy 2
				#$othersel set beta -1
				set fout [writenamdconf $namdconfigfile $deletelines]
				if { ! $finished } {
					$badbeta update
					mdffi sim $badbeta -o grid.dx -res 10 -spacing 1 -mol $mid
					if { [molinfo top get numframes] == 0 } {
						#Versions of 1.9.4 will load the volume into its own molecule, which we delete here.
						mol delete top
					}
					if { $genchirality } {
						puts $fout "extraBonds on\n extraBondsFile lbe-extrabonds.txt\n"
					}
					puts $fout "alch on\nalchType ti\nalchFile tmp-lbe.pdb\nalchLambda 0.02\nalchLambda2 0.02\n"
					puts $fout "gridForce on\ngridForceFile tmp-lbe.pdb\ngridForceCol B\ngridForceChargeCol O\ngridForcePotFile grid.dx\ngridForceScale $gc $gc $gc\n"
					if { $largestdeviation > 0.5 || [$othersel num] } {
						puts $fout "colvars on"
						if { $largestdeviation > 0.5 } {
							set dummypos [forcelongestbond $mid $largestloc]
							puts $fout "cv config \"colvar {\n  name d\n upperWall 0\nupperWallConstant 100\n distance {\n    group1 {\n      atomNumbers {\n        $largestloc\n      }\n    }\n    group2 {\n      dummyAtom $dummypos\n    }\n  }\n}\n\""
						}
						if { [$othersel num] } {
							puts $fout "cv config \"colvar {\nname contact\nupperWall 0\nupperWallConstant $wallconstant\ncoordNum {\ngroup1 {\n atomsFile tmp-lbe.pdb\natomsCol O\natomsColValue 1.0\n  }\n group2 {\n atomsFile tmp-lbe.pdb\natomsCol O\natomsColValue 2.0\n}\ncutoff 3\ntolerance 0.001\n}\n}\n\""
						}
					}
					
				}

				if { $unfinished } {
					set finished 0
				}
				if { ! $finished && $counter >= 100 } {
					$asel writenamdbin lbe-last.coor
					error "I'm sorry, I don't seem to be able to minimize the system, and I tried 100 times!"
				}
				if { $writeintemediates } {
					$asel writenamdbin [format "lbe-%02d.coor" $counter]
				}
				#Very large systems will overflow their x, y, or z coordinates.
				$asel set x 0
				$asel set y 0
				$asel set z 0
				$asel writepdb tmp-lbe.pdb
				puts $fout "reinitatoms $outputname"
				puts $fout "minimize 500"
				close $fout
			}
			set cutoff [expr { $largestdeviation - 0.1 }]
		}
		$asel writenamdbin lbe-opt.coor
		$asel delete
		$othersel delete
		$badbeta delete
	} 
	#This function takes the essential part from minimize to determine how bad a given structure is, returning the largest bond deviation and where it happens.
	proc writelargestdeviation { molid parameterlist filename } {
		set fout [open $filename "w"]
		puts $fout "#Frame Largestdeviation(Angstroms) index1 index2"
		set asel [atomselect $molid "all"]
		set atypes [$asel get type]
		set bonddict [buildbondtable $parameterlist]
		for { set frame 0 } { $frame < [molinfo $molid get numframes] } { incr frame } {
			set largestdeviation -10.0
			foreach bond [topo getbondlist -molid $molid] {
				set bondkey [join [lsort [ list [lindex $atypes [lindex $bond 0]] [lindex $atypes [lindex $bond 1]]]] "_"]
				set bonddiff [expr { [measure bond $bond molid $molid frame $frame] - [dict get $bonddict $bondkey] } ]
				if { $bonddiff > $largestdeviation } {
					set largestdeviation $bonddiff
					set largestloc $bond
				}
			}
			puts $fout "$frame $largestdeviation $largestloc"
		}
		
		$asel delete
		close $fout
	}
	proc tagdeviation { molid parameterlist } {
		set bonddict [buildbondtable $parameterlist]
		set asel [atomselect $molid "all"]
		for { set frame 0 } { $frame < [molinfo $molid get numframes] } { incr frame } {
			$asel frame $frame
			$asel set user 0
			set atypes [$asel get type]
			foreach bond [topo getbondlist -molid $molid] {
				set bondkey [join [lsort [ list [lindex $atypes [lindex $bond 0]] [lindex $atypes [lindex $bond 1]]]] "_"]
				set bonddiff [expr { [measure bond $bond molid $molid frame $frame] - [dict get $bonddict $bondkey] } ]
				if { $bonddiff > 0.1 } {
					set bsel [atomselect $molid "index $bond" frame $frame]
					set betas [$bsel get user]
					set newbetas [list ]
					foreach b $betas {
						lappend newbetas [expr {$b + $bonddiff}]
					}
					$bsel set user $newbetas
					$bsel delete
				}
			}
		}
		$asel delete
	}
	proc minimize { namdconfigfile { deletelines 3 } { namdrunargs "namd2" } { cutoff 0.4 } { genchirality 1 } { writeintemediates 0 } } {
		set cwd [pwd]
		cd [file dirname $namdconfigfile]
		#Read in the current input structure
		lassign [parseinputfile [file tail $namdconfigfile]] structure coordinates parameters outputname
		puts "$structure $coordinates"
		set mid [loadsystem $structure $coordinates]
		set bonddict [buildbondtable $parameters]
		runloop [file tail $namdconfigfile] $deletelines $mid $bonddict $namdrunargs $outputname $cutoff $genchirality $writeintemediates 
		cd $cwd
	}
}
