#/usr/bin/env python
import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}",
r"\usepackage{fixltx2e}", r'\usepackage{helvet}',r'\renewcommand{\familydefault}{\sfdefault}',r'\renewcommand{\familydefault}{\sfdefault}',r'\usepackage{mathastext}']
params = {'text.usetex' : True,
		  'font.size' : 16
		  }
matplotlib.rcParams.update(params) 
import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots(1,1)
labels = ['Lignin', 'Aggrecan', 'ChAdOX1']
for i, name in enumerate(['lignin', 'aggrecan', 'virus']):
	data = np.loadtxt(f"{name}-deviation.txt", usecols=(0,1))
	ax.plot(data[:,1], label=labels[i], marker='.', linewidth=1,zorder=3)
ax.annotate("%.1f\\,\\AA" % data[0,1], (1,2.5), xytext=(1,2.2), arrowprops=dict(arrowstyle="-|>", color='C2',ls='-'), ha='left', color='C2')
ax.legend()
ax.axhline(0.4, ls='--', color='gray',zorder=2)
ax.set_xlabel("Step")
ax.set_ylabel("Largest Bond Deviation (\\AA)")
ax.set_ylim(bottom=0,top=2.5)
ax.set_xlim(left=-0.1)
fig.tight_layout()
fig.savefig("deviationplot.png")
fig.savefig("deviationplot.pdf")