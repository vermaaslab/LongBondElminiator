from PIL import Image, ImageFont, ImageDraw
import os
import glob
import numpy as np
for name in ['aggrecan', 'lignin', 'virus']:
	filelist = sorted(glob.glob("%s.*.png" % name))
	font = ImageFont.truetype("/usr/share/fonts/truetype/ubuntu/UbuntuMono-R.ttf", 64)
	for i, f in enumerate(filelist):
		image = Image.open(f)
		text = "Step %d" % i
		newframe = ImageDraw.Draw(image)
		newframe.text((920,1105), text, (0,0,0), font=font)
		image.save("new-%s.%02d.png" % (name, i))