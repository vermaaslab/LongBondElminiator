source ../../vmdplugin/longbondeliminator.tcl
proc addtraj { name } {
	set coorlist [lsort [glob lbe*coor]]
	foreach f $coorlist {
		mol addfile $f type namdbin waitfor -1
	}
}
proc getnamdconfigfile { name } {
	if { $name == "aggrecan" } {
		return "../$name/system_min_fix.namd"
	} else {
		return "../$name/minimize.namd"
	}
}
foreach system [list lignin aggrecan virus] { 
	set cwd [pwd]
	puts $cwd
	set namdconfigfile [getnamdconfigfile $system]
	cd [file dirname $namdconfigfile]
	lassign [::longbondeliminator::parseinputfile [file tail $namdconfigfile]] structure coordinates parameters outputname
	set mid [::longbondeliminator::loadsystem $structure $coordinates]
	addtraj $system
	#::longbondeliminator::tagdeviation $mid $parameters
	::longbondeliminator::writelargestdeviation $mid $parameters "$cwd/$system-deviation.txt"
	cd $cwd
}
