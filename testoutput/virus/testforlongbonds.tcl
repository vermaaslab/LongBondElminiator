mol load js tmp.js namdbin out.coor
package require topotools
set counter 0
foreach bond [topo getbondlist] {
        set i [lindex $bond 0]
        set j [lindex $bond 1]
        if { [measure bond $bond] > 2 } {
        puts "$i $j [measure bond $bond]"
        incr counter
        }
}
puts "$counter"